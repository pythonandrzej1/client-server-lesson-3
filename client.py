import socket
import json

HOST = "127.10.12.1"
PORT = 30000

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    
    while True:
    
        client_socket.send(json.dumps(input()).encode("utf8"))
        server_answer = json.loads((client_socket.recv(1024).decode("utf8")))
        print(server_answer)
