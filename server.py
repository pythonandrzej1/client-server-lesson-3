import socket
import json
import csv
from pathlib import Path

help_ =  "1 - register\n2 - login,\n'close' - to terminate connection"
write_message_questions = ["To who You want to write message?","Please type the message"]
login_questions = ["Please type 4'Your login'", "Please type Your pas", "Login incorrect\n1 - register\n2 - login,\n3 - read message,\n4 write and send message,\n'close' - to terminate connection",\
                   "Password incorrect\n1 - register\n2 - login,\n3 - read message,\n4 write and send message,\n'close' - to terminate connection",\
                   "Logged in, 5 - to write message, 6 - to read message"]
admin_questions = ["Type 1 to change user password\nType 2 to delete user\nType 3 to write message\nType 4 to read message"]

HOST = "127.10.12.1"
PORT = 30000

def registration(client_request):
    client_answer = client_request.partition('#')
    register_data = [{"login": str(client_answer[0][1:]), "password": str(client_answer[2])}]
    with open(file_path_1, mode = "a", encoding = "utf-8", newline = "") as file:
        writer = csv.DictWriter(file, fieldnames = ["login", "password"])
        writer.writeheader()
        writer.writerows(register_data)
    return str("Registration completed")

def write_message(sender):
    client_answer_message = []
    if_written = "Message send"
    for server_answer in write_message_questions:
        client_socket.send(json.dumps(server_answer).encode("utf8"))
        client_answer_message.append(json.loads((client_socket.recv(1024).decode("utf8"))))
    message_content = [{"sender": sender,"message": str(client_answer_message[1])[0:255], "recipient": str(client_answer_message[0]), "if_read": "Not read"}]
    with open(file_path_2, mode = "r", encoding = "utf-8", newline = "") as file:
        reader = csv.DictReader(file)
        number_of_not_read = 0
        for row in reader:
            if row["recipient"] == str(client_answer_message[0]) and row["if_read"] == "Not read":
                number_of_not_read += 1
        if number_of_not_read < 5:
            with open(file_path_2, mode = "a", encoding = "utf-8", newline = "") as file:
                    writer = csv.DictWriter(file, fieldnames = ["sender", "message", "recipient","if_read"]) 
                    writer.writeheader()
                    writer.writerows(message_content)
        else:
            if_written = "Mailbox of the receipent is full. The message has not been sent"
    return if_written 

def read_message(reading_user):
    if_message_read = "Message read"
    messages_database = []
    with open(file_path_2, mode = "r", encoding = "utf-8", newline = "") as file:
        reader = csv.DictReader(file)
        for row in reader:
            messages_database.append(row)
        for row in messages_database:
            if row["recipient"] == str(reading_user) and row["if_read"] == "Not read":
                client_socket.send(json.dumps(f"Message from {row['sender']}\n{row['message']}\nDo You want to read next unread one?").encode("utf8"))
                row["if_read"] = "read"
                with open(file_path_2, mode = "w", encoding = "utf-8", newline = "") as file:
                    writer = csv.DictWriter(file, fieldnames = ["sender", "message", "recipient","if_read"]) 
                    writer.writeheader()
                    writer.writerows(messages_database)
                if json.loads((client_socket.recv(1024).decode("utf8"))) == str("yes"):
                    pass
                else:
                    break
    return if_message_read

def admin(): #Admin permitted to delete any user and to change password of any user
    client_socket.send(json.dumps(admin_questions[0]).encode("utf8"))
    admin_answer = str(json.loads((client_socket.recv(1024).decode("utf8"))))
    if admin_answer == "1":
        users_database = []
        client_socket.send(json.dumps("Provide user name to change password").encode("utf8"))
        user_to_be_changed = str(json.loads((client_socket.recv(1024).decode("utf8"))))
        with open(file_path_1, mode = "r", encoding = "utf-8", newline = "") as file:
            reader = csv.DictReader(file)
            for row in reader:
                users_database.append(row)
            for row in users_database:
                if row["login"] == str(user_to_be_changed):
                    client_socket.send(json.dumps("Provide new password").encode("utf8"))
                    row["password"] = str(json.loads((client_socket.recv(1024).decode("utf8"))))
                    with open(file_path_1, mode = "w", encoding = "utf-8", newline = "") as file:
                        writer = csv.DictWriter(file, fieldnames = ["login", "password"]) 
                        writer.writeheader()
                        writer.writerows(users_database)
                    break
                else:
                    pass
    elif admin_answer == "2":
        users_database = []
        client_socket.send(json.dumps("Provide user name to be deleted").encode("utf8"))
        user_to_be_deleted = str(json.loads((client_socket.recv(1024).decode("utf8"))))
        with open(file_path_1, mode = "r", encoding = "utf-8", newline = "") as file:
            reader = csv.DictReader(file)
            for row in reader:
                users_database.append(row)
            for row in users_database:
                if row["login"] == str(user_to_be_deleted):
                    row["login"] = ""
                    row["password"] = ""
                    with open(file_path_1, mode = "w", encoding = "utf-8", newline = "") as file:
                        writer = csv.DictWriter(file, fieldnames = ["login", "password"]) 
                        writer.writeheader()
                        writer.writerows(users_database)
                    break
                else:
                    pass
    elif admin_answer == "3":
        write_message("admin")
    elif admin_answer == "4":
        read_message("admin")
    return client_socket.send(json.dumps("Logout as admin").encode("utf8"))

def main(client_request):
    if client_request == "help":
        client_choice = help_
    elif client_request == "1":
        client_choice = "To register type:3'Your login'#'Your password'"
    elif client_request[0] == "3":
        client_choice = registration(client_request) #registration(client_request)
    elif client_request == "2":
        client_choice = "To log in type:4'Your login'#'Your password'"
    elif client_request[0] == "4":
        client_choice = login_check(client_request)
    elif client_request == "close":
        client_choice = "Connection terminated"
    elif client_request == "5":
        client_choice = "Lets write something"
    else:
        client_choice = "Welcome, please type what action You want to call? (Type 'help' for options)"
    return client_choice

def login_check(client_request):
    login_answer = client_request.partition('#')
    login_check_result = "Login incorrect. You are back to main manu"
    with open(file_path_1, mode = "r", encoding = "utf-8", newline = "") as file:
        reader = csv.DictReader(file)
        for row in reader:
            if str(login_answer[0][1:]) in row["login"]:
                login_check_result = "Login correct"
                if str(login_answer[2]) in row["password"]:
                    login_check_result = "You are logged in, Please type 5 to write message, 6 to read message"
                    logged_data = [{"login": str(login_answer[0][1:])}]
                    with open(file_path_3, mode = "w", encoding = "utf-8", newline = "") as file:
                        writer = csv.DictWriter(file, fieldnames = ["login"])
                        writer.writeheader()
                        writer.writerows(logged_data)
                else:
                    login_check_result = "Password incorrect. You are back to main manu"
    return login_check_result



file_path_1 = "C:/Users/ANDRZEJ/Desktop/registrations.json"
file_path_2 = "C:/Users/ANDRZEJ/Desktop/messages.json"
file_path_3 = "C:/Users/ANDRZEJ/Desktop/current_logged_in.json"
Path(file_path_3).unlink()
Path(file_path_1,file_path_2,file_path_3).touch()
client_request = ""

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
    server_socket.bind((HOST, PORT))
    server_socket.listen()
    client_socket, address = server_socket.accept()
        
    while client_request != "close":
       client_request = str(json.loads((client_socket.recv(1024).decode("utf8"))))
       client_socket.send(json.dumps(main(client_request)).encode("utf8"))       
        
        
       
        
    
